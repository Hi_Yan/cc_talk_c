#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void menu()
{
	printf("********************\n");
	printf("**** 猜数字游戏 ****\n");
	printf("********************\n");
	printf("*****  1.play  *****\n");
	printf("*****  0.exit  *****\n");
	printf("********************\n");
}

void game()
{
	int count = 5;
	int r = rand() % 100 + 1;//随机数范围1-100
	printf("游戏规则：系统会生成一个1-100的随机数字，由你的聪明小脑瓜子来猜一猜！\n");
	while (count)
	{
		int ans = 0;
		printf("请输入数字：");
		scanf("%d", &ans);
		if (ans < r)
		{
			printf("猜小了\n");
		}
		else if (ans > r)
		{
			printf("猜大了\n");
		}
		else
		{
			printf("恭喜你，猜对了！\n");
			break;
		}
		count--;
	}
	if (count == 0)
	{
		printf("您的五次试用机会已用光！！！\n");
		printf("限时优惠，首充六元，解锁无限次数版！！！\n");
		printf("机不可失，失不再来！！！\n");
		printf("心动不如行动，赶紧联系管理员进行充值吧！！！\n");
	}
}

int main()
{
	int input = 0;
	srand((unsigned int)time(NULL));
	do
	{
		menu();
		printf("请选择：");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			game();
			break;
		case 0:
			printf("游戏结束");
			break;
		default:
			printf("您的输入有误，请重新输入\n");
			break;
		}
	} while (input);
	return 0;
}