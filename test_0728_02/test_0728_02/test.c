#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

// 求数组中元素的最大值
int GetMaxValOfArray(int arr[], int n) 
{
    int maxVal = arr[0]; // 假设第一个元素为最大值

    for (int i = 1; i < n; i++) {
        if (arr[i] > maxVal) {
            maxVal = arr[i];
        }
    }
    return maxVal;
}

// 求数组中元素的最小值
int GetMinValOfArray(int arr[], int n) 
{
    int minVal = arr[0]; // 假设第一个元素为最小值

    for (int i = 1; i < n; i++) {
        if (arr[i] < minVal) {
            minVal = arr[i];
        }
    }
    return minVal;
}

// 打印数组中的所有元素
void PrintArray(int arr[], int n)
{
    printf("数组中的元素为：");
    for (int i = 0; i < n; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

int main() 
{
    int n = 0;
    int arr[] = {1,2,3,4,5,6,7,8,9,10};
    
    n = sizeof(arr) / sizeof(arr[0]);

    int maxVal = GetMaxValOfArray(arr, n);
    int minVal = GetMinValOfArray(arr, n);

    printf("最大值: %d\n", maxVal);
    printf("最小值: %d\n", minVal);
    PrintArray(arr, n);
    printf("黄煜东上机打卡");
    return 0;
}