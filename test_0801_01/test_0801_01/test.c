#define _CRT_SECURE_NO_WARNINGS 1
//#include <stdio.h>
//
//int main() {
//    int n, m;
//    scanf("%d %d", &n, &m);
//    int arr1[10];
//    int arr2[10];
//    int i = 0;
//    int j = 0;
//    for (i = 0; i < n; i++) {
//        scanf("%d", &arr1[i]);
//    }
//    for (j = 0; j < m; j++) {
//        scanf("%d", &arr2[j]);
//    }
//
//    for (i = 0; i < n; i++)
//    {
//        while (j < m)
//        {
//            if (arr1[i] < arr2[j])
//            {
//                printf("%d ", arr1[i]);
//                break;
//            }
//            else if (arr1[i] > arr2[j])
//            {
//                printf("%d ", arr2[j]);
//                j++;
//            }
//            else
//            {
//                printf("%d %d ", arr1[i], arr2[j]);
//                break;
//            }
//        }
//    }
//    return 0;
//}



////实现一个函数is_prime，判断一个数是不是素数。
////利用上面实现的is_prime函数，打印100到200之间的素数。
//#include <stdio.h>
//#include <math.h>
//
//int is_prime(int x)
//{
//	int i = 0;
//	for (i = 2; i <= sqrt(x); i++)
//	{
//		if (x % i == 0)
//		{
//			return 0;
//		}
//	}
//	return 1;
//}
//
//int main()
//{
//	int i = 0;
//	int flag = 0;
//	for (i = 100; i <= 200; i++)
//	{
//		flag = is_prime(i);
//		if (flag == 1)
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}




////实现函数判断year是不是润年。
//#include <stdio.h>
//
//void year(int x)
//{
//	if (x % 400 == 0 || (x % 4 == 0 && x % 100 != 0))
//	{
//		printf("是闰年");
//	}
//	else
//		printf("不是闰年");
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	year(n);
//	return 0;
//}




//实现函数init() 初始化数组为全0
//实现print()  打印数组的每个元素
//实现reverse()  函数完成数组元素的逆置。
//#include <stdio.h>
//
//
//
//int main()
//{
//	return 0;
//}




////实现一个函数，打印乘法口诀表，口诀表的行数和列数自己指定
////如：输入9，输出9 * 9口诀表，输出12，输出12 * 12的乘法口诀表。
//#include <stdio.h>
//
//void mathmatic(int x)
//{
//	int i = 0;
//	int j = 0;
//	for (i = 1; i <= x; i++)
//	{
//		for (j = i; j <= x; j++)
//		{
//			printf("%d*%d=%d  ", i, j, i * j);
//		}
//		printf("\n");
//	}
//}
//
//int main()
//{
//	int x = 0;
//	scanf("%d", &x);
//	mathmatic(x);
//	return 0;
//}





//写一个二分查找函数
//功能：在一个升序数组中查找指定的数值，找到了就返回下标，找不到就返回 - 1.
//int bin_search(int arr[], int left, int right, int key)
// arr 是查找的数组
//left 数组的左下标
//right 数组的右下标
//key 要查找的数字
#include <stdio.h>

int bin_search(int arr[], int left,int right,int key)
{
	while (left <= right)
	{
		int mid = left+(right-left)/2;
		if (arr[mid] < key)
		{
			left = mid + 1;
		}
		else if (arr[mid] > key)
		{
			right = mid - 1;
		}
		else
		{
			return mid;
		}
	}
	return -1;
}

int main()
{
	int arr[] = {1,2,3,4,5,6,7,8,9,10};
	int n = 0; 
	int left = 0;
	int sz = sizeof(arr) / sizeof(arr[0]);
	int right = sz - 1;
	scanf("%d", &n);
	int ret = bin_search(arr,left,right,n);
	if (ret != -1)
	{
		printf("找到了，您要查找的数字在数组中的下标为%d", ret);
	}
	else
	{
		printf("抱歉，没有找到您想要的数字");
	}
	return 0;
}