#define _CRT_SECURE_NO_WARNINGS 1
//递归和非递归分别实现求第n个斐波那契数,1,1,2,3,5,8,13,21,34,55...
//例如：输入：5  输出：5输入：10， 输出：55输入：2， 输出：1

////递归：
//#include <stdio.h>
//
//int Feb(int n)
//{
//	if (n == 1 || n == 2)
//		return 1;
//	else
//		return Feb(n - 1) + Feb(n - 2);
//}
//
//int main()
//{
//	int n = 0;
//	while (scanf("%d", &n) > 0)
//	{
//		int ret = Feb(n);
//		printf("%d\n", ret);
//	}
//	return 0;
//}



////非递归
//#include <stdio.h>
//
//int Feb(int n)
//{
//	int a = 1;
//	int b = 1;
//	int c = 1;
//	while (n>2)
//	{
//		c = a + b;
//		a = b;
//		b = c;
//		n--;
//	}
//	return c;
//}
//
//int main()
//{
//	int n = 0;
//	while (scanf("%d", &n) > 0)
//	{
//		int ret = Feb(n);
//		printf("%d\n", ret);
//	}
//	return 0;
//}





////编写一个函数实现n的k次方，使用递归实现。
//#include <stdio.h>
//
//int Mat(int x, int y)
//{
//	if (y == 0)
//		return 1;
//	else if (y == 1)
//		return x;
//	else
//		return Mat(x, y - 1) * x;
//}
//
//int main()
//{
//	int n = 0;
//	int k = 0;
//	scanf("%d %d", &n, &k);
//	int ret = Mat(n, k);
//	printf("%d\n", ret);
//	return 0;
//}




////写一个递归函数DigitSum(n)，输入一个非负整数，返回组成它的数字之和
////输入：1729，输出：19
//#include <stdio.h>
//
//int DigitSum(int n)
//{
//	int a = 0;
//	if (n > 0)
//	{
//		a = (n % 10) + DigitSum(n / 10);
//	}
//	return a;
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int ret = DigitSum(n);
//	printf("%d\n", ret);
//	return 0;
//}






//递归和非递归分别实现求n的阶乘（不考虑溢出的问题）
////递归
//#include <stdio.h>
//
//int Mat(int n)
//{
//	if (n == 0)
//		return 1;
//	return n * Mat(n - 1);
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int ret = Mat(n);
//	printf("%d\n", ret);
//	return 0;
//}


////非递归
//#include <stdio.h>
//
//int Mat(int n)
//{
//	int a = 1;
//	while (n)
//	{
//		a *= n;
//		n--;
//	}
//	return a;
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int ret = Mat(n);
//	printf("%d\n", ret);
//	return 0;
//}





////递归方式实现打印一个整数的每一位
//#include <stdio.h>
//
//void Print(int x)
//{
//	int a = 0;
//	if (x != 0)
//	{
//		a = x % 10;
//		Print(x / 10);
//		printf("%d ", a);
//	}
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	Print(n);
//	return 0;
//}




//#include<stdio.h>
//int main()
//{
//	int arr[3][5] = {{1, 2, 3, 4, 5}, { 2, 3, 4, 5, 6 }, { 3, 4, 5, 6, 7 }};
//	//printf("%d\n”,arr[1][3]); //[]下标引用操作符
//	int i = 0;
//	for (i = 0; i < 3; i++)
//	{
//		int j = 0; 
//		for (j = 0; j < 5; j++)
//		{
//			printf("%d", arr[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}



//写一个代码，判断一个数是否是2^n次方？
//2^n 这个数的二进制中只有一个1
//
//
//int main()
//{
//
//	int n = 0;
//	scanf("%d", &n);
//	if ((n & (n - 1)) == 0)
//	{
//		printf("yes\n");
//	}
//	else
//	{
//		printf("no\n");
//	}
//
//	return  0;
//}





//~ -> 按位取反操作符(二进制是0的变成1，是1的变成0)

//
//int main()
//{
//	//int a = 13;
//	////00000000000000000000000000001101
//	////00000000000000000000000000010000
//	////1<<4
//	////将a的二进制中第5位改成1
//	//a = a | (1 << 4);
//	//printf("a = %d\n", a);
//	////将a的二进制中第5位改成0
//	////00000000000000000000000000011101
//	////11111111111111111111111111101111
//	////00000000000000000000000000001101
//	//a = a & ~(1 << 4);
//	//printf("a = %d\n", a);






//在一个整型数组中，只有一个数字出现一次，其他数组都是成对出现的，请找出那个只出现一次的数字。
//数组中有：1 2 3 4 5 1 2 3 4，只有5出现一次，其他数字都出现2次，找出5
#include <stdio.h>

//int main()
//{
//	int arr[] = { 1 , 2 , 3 , 4 , 5 , 1 , 2 , 3 , 4 };
//	int num = sizeof(arr) / sizeof(arr[0]);
//	int i = 0;
//	int res = 0;
//	for (i = 0; i < num; i++)
//	{
//		res = res ^ arr[i];
//	}
//	printf("%d\n", res);
//	return 0;
//}





//写一个函数返回参数二进制中 1 的个数。
//比如： 15    0000 1111    4 个 1
//#include <stdio.h>
//int main()
//{
//	int a = 0;
//	int count = 0;
//	scanf("%d", &a);
//	while (a)
//	{
//		a = a & (a - 1);
//		count++;
//	}
//	printf("%d个1\n", count);
//	return 0;
//}





//不允许创建临时变量，交换两个整数的内容
#include <stdio.h>
int main()
{
	int a = 3;
	int b = 5;
	printf("交换前：a=%d,b=%d\n", a, b);
	a = a ^ b;
	b = a ^ b;
	a = a ^ b;
	printf("交换后：a=%d,b=%d\n", a, b);
	return 0;
}